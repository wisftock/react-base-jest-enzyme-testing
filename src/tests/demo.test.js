describe('Prueba en Demo', () => {
  test('Deben ser iguales los string', () => {
    // 1 inicializacion
    const mensaje = 'hola mundo';
    // 2 estimulo
    const mensaje2 = `hola mundo`;
    // observar el comportamiento
    expect(mensaje).toBe(mensaje2);
  });
});
