import '@testing-library/jest-dom';

import PrimeraApp from '../PrimeraApp';
import { shallow } from 'enzyme';
describe('Prueba en PrimeraApp.js', () => {
  //   test('debe de mostrar el mensaje: hola mundo', () => {
  //     const saludo = ' hola mundo';
  //     const { getByText } = render(<PrimeraApp saludo={saludo} />);
  //     expect(getByText(saludo)).toBeInTheDocument();
  //   });
  test('debe mostrar <PrimeraApp /> correctamente', () => {
    const saludo = 'hola mundo';
    const wrapper = shallow(<PrimeraApp saludo={saludo} />);
    expect(wrapper).toMatchSnapshot();
  });

  test('debe de mostrar el subtitulo enviado por props', () => {
    const saludo = 'hola mundo';
    const subtitulo = 'Soy un subtitulo';
    const wrapper = shallow(
      <PrimeraApp saludo={saludo} subtitulo={subtitulo} />
    );
    // buscando un parrafo en el wrapper, el .find equivale al document.querySelector()
    const textoParrafo = wrapper.find('h3').text();
    // console.log(textoParrafo);
    expect(textoParrafo).toBe(subtitulo);
  });
});
