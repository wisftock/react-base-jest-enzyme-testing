// import '@testing-library/jest-dom';
// import { logDOM } from '@testing-library/react';
import React from 'react';
import { shallow } from 'enzyme';
import CounterApp from '../CounterApp';

describe('Prueba en <CounterApp />', () => {
  let wrapper = shallow(<CounterApp />);
  // antes de todas
  beforeEach(() => {
    wrapper = shallow(<CounterApp />);
  });
  test('tomar un snapschot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  test('asignar el value a 100', () => {
    const value = 100;
    const wrapper = shallow(<CounterApp value={value} />);
    const valueText = wrapper.find('h3').text().trim();

    expect(valueText).toBe(`${value}`);
  });

  test('debe de incrementar contador +1', () => {
    wrapper.find('button').at(2).simulate('click');
    // console.log(btn1.html());
    const valueText = wrapper.find('h3').text().trim();
    expect(valueText).toBe('11');
  });

  test('debe de decrementar contador -1', () => {
    wrapper.find('button').at(0).simulate('click');
    // console.log(btn1.html());
    const valueText = wrapper.find('h3').text().trim();
    expect(valueText).toBe('9');
  });

  test('debe colocar el valor por defecto con el btn reset', () => {
    const wrapper = shallow(<CounterApp value={150} />);
    wrapper.find('button').at(2).simulate('click');

    wrapper.find('button').at(1).simulate('click');
    const valueText = wrapper.find('h3').text().trim();

    expect(valueText).toBe('150');
  });
});
