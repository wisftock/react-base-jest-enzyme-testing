import "@testing-library/jest-dom";
import { getHeroeById, getHeroesByOwner } from "../../base-pruebas/08-imp-exp";
import heroes from "../../data/heroes";
describe("Pruebas en funciones de herores", () => {
  test("debe de retornar un heroe por id", () => {
    const id = 1;
    const heroe = getHeroeById(id);
    // console.log(heroe);
    const heroesData = heroes.filter((h) => h.id === id);
    // console.log(heroesData);
    expect([heroe]).toEqual(heroesData);
  });

  test("debe de retornar undefined si el heroe no existe", () => {
    const id = 10;
    const heroe = getHeroeById(id);
    // console.log(heroe);
    expect(heroe).toBe(undefined);
  });

  test("debe de retornar un arreglo con lo heroes de DC", () => {
    const owner = "DC";
    const owners = getHeroesByOwner(owner);
    // console.log(owners);
    const ownerData = heroes.filter((ow) => ow.owner === owner);
    // console.log(ownerData);
    expect(owners).toEqual(ownerData);
  });

  test("debe de retornar un arreglo con los heroes de marvel", () => {
    const owner = "Marvel";
    const owners = getHeroesByOwner(owner);
    // console.log(owners.length);
    expect(owners.length).toBe(2);
  });
});
