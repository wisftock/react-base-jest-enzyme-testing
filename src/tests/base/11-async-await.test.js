import '@testing-library/jest-dom';
import getImagen from '../../base-pruebas/11-async-await';
describe('Pruebas con async await y fetch', () => {
  test('Debe de retornar el url de la imagen', async () => {
    const url = await getImagen();
    // console.log(url);
    // verificando si la url es un string
    // expect(typeof url).toBe('string');
    // que incluya https
    expect(url.includes('https://')).toBe(true);
  });
});
