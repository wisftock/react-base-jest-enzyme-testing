import '@testing-library/jest-dom';
import { getSaludo } from '../../base-pruebas/02-template-string';

describe('Prueba en 02-template-string', () => {
  test('getSaludo debe de retornar hola nombre', () => {
    const nombre = 'Pepe';
    const saludo = getSaludo(nombre);
    expect(saludo).toBe('Hola ' + nombre + '!');
  });

  //  getSaludo debe de retornar hola carlos si no hay argumentos nombre
  test('getSaludo debe retornar nombre si no tiene valores', () => {
    const nombre = 'Pepe';
    const saludo = getSaludo();
    expect(saludo).toBe('Hola ' + nombre + '!');
  });
});
