import '@testing-library/jest-dom';
import { retornaArreglo } from '../../base-pruebas/07-deses-arr';

describe('Prueba en 07-deses-arr.js', () => {
  test('debe de retornar un string y un numero', () => {
    const userTest = ['ABC', 123];
    const [letras, numeros] = retornaArreglo();
    const arreglo = retornaArreglo();

    expect(arreglo).toEqual(userTest);

    expect(letras).toBe('ABC');
    expect(typeof letras).toBe('string');

    expect(numeros).toBe(123);
    expect(typeof numeros).toBe('number');
  });
});
