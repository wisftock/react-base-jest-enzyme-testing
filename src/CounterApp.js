import React, { useState } from 'react';
import PropTypes from 'prop-types';

const CounterApp = ({ value = 10 }) => {
  const [counter, setCounter] = useState(value);

  const handleSuma = () => {
    // setCounter(counter + 1);
    setCounter((c) => c + 1);
  };
  const handleResta = () => {
    setCounter(counter - 1);
  };
  const handleIgual = () => {
    setCounter(value);
  };
  return (
    <div>
      <h2>CounterApp</h2>
      <h3>{counter}</h3>
      <button onClick={handleResta}>-1</button>
      <button onClick={handleIgual}>Reset</button>
      <button onClick={handleSuma}>+1</button>
    </div>
  );
};
CounterApp.propTypes = {
  value: PropTypes.number,
};
export default CounterApp;
